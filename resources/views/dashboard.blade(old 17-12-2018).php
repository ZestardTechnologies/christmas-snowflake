@extends('header')
@section('content')
<?php 
    $store_name = session('shop');
?>

<script type="text/javascript">
  ShopifyApp.ready(function(){
    ShopifyApp.Bar.initialize({
      buttons: {
        primary: {
          label: 'SAVE SETTINGS',
          message: 'form_submit',
          loading: true,
        },
        secondary: {
          label: 'HELP',
          href : '{{ url('/help') }}',
          loading: false
        }
      }
    });
  });
</script>

<div class="dashboard">
    <div class="christmas-snowflake-container">
        <!-- snow settings -->
        <form id="christmas-snowflake" method="post" data-shopify-app-submit="form_submit" data-toggle="validator" action="{{ url('save') }}">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
        <div class="col-md-6 left-christmas-snowflake">
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">App Settings</h2>                    
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-6 form-group">
                        <label for="app-status">App Enabled?</label>
                        <select class="form-control" id="app-status" name="app-status">
                            <option @if(count($store_record)> 0) value="1"
                                <?php echo ($store_record->app_status == '1' ? ' selected' : '') ?>@else value="1" @endif>Enabled</option>
                            <option @if(count($store_record)> 0) value="0"
                                <?php echo ($store_record->app_status == '0' ? ' selected' : '') ?>@else value="0" @endif>Disabled</option>
                        </select>
                    </div>
                    <div class="col-md-6 form-group">
                        <label for="select_page">Select Page?</label>
                        <select class="form-control" id="select_page" name="select_page">
                            <option @if(count($store_record)> 0) value="0"
                            <?php echo ($store_record->select_page == '0' ? ' selected' : '') ?>@else value="0" @endif>Home Page</option>
                            <option @if(count($store_record)> 0) value="1"
                            <?php echo ($store_record->select_page == '1' ? ' selected' : '') ?>@else value="1" @endif>All Page</option>
                        </select>
                    </div>                       
                </div>
            </div>
            
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">Snow Settings</h2>
                <div class="col-md-12 form-group section-group">                         
                    <div class="col-md-6 form-group">
                        <label for="show_snow">Show snow?</label>
                        <select class="form-control" id="show_snow" name="show_snow">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_snow == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_snow == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option> 
                        </select>
                    </div>
                        
                    <div class="col-md-6 form-group">
                        <label for="snow_images">Select Snow Image</label>                            
                        <?php
                            if(count($store_record)> 0){
                                $image_data = $store_record->snow_images;                                
                                if($image_data == FALSE){                                    
                                    $snow_images = array();                                    
                                } else {
                                    $snow_images = json_decode($image_data);
                                }                                                                
                            }
                        ?>
                        <select id="snow_images" name="snow_images[]" class="form-control" size="7" multiple="multiple">                               
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow1.png" style="background:url('image/snow1.png') no-repeat;" <?php echo in_array("snow1.png" , $snow_images) ? ' selected' : '' ?>@else value="snow1.png" style="background:url('image/snow1.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow2.png" style="background:url('image/snow2.png') no-repeat;" <?php echo in_array("snow2.png" , $snow_images) ? ' selected' : '' ?>@else value="snow2.png" style="background:url('image/snow2.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow3.png" style="background:url('image/snow3.png') no-repeat;" <?php echo in_array("snow3.png" , $snow_images) ? ' selected' : '' ?>@else value="snow3.png" style="background:url('image/snow3.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow4.png" style="background:url('image/snow4.png') no-repeat;" <?php echo in_array("snow4.png" , $snow_images) ? ' selected' : '' ?>@else value="snow4.png" style="background:url('image/snow4.png') no-repeat;" selected @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow5.png" style="background:url('image/snow5.png') no-repeat;" <?php echo in_array("snow5.png" , $snow_images) ? ' selected' : '' ?>@else value="snow5.png" style="background:url('image/snow5.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow6.png" style="background:url('image/snow6.png') no-repeat;" <?php echo in_array("snow6.png" , $snow_images) ? ' selected' : '' ?>@else value="snow6.png" style="background:url('image/snow6.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow7.png" style="background:url('image/snow7.png') no-repeat;" <?php echo in_array("snow7.png" , $snow_images) ? ' selected' : '' ?>@else value="snow7.png" style="background:url('image/snow7.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow8.png" style="background:url('image/snow8.png') no-repeat;" <?php echo in_array("snow8.png" , $snow_images) ? ' selected' : '' ?>@else value="snow8.png" style="background:url('image/snow8.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow9.png" style="background:url('image/snow9.png') no-repeat;" <?php echo in_array("snow9.png" , $snow_images) ? ' selected' : '' ?>@else value="snow9.png" style="background:url('image/snow9.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow10.png" style="background:url('image/snow10.png') no-repeat;" <?php echo in_array("snow10.png" , $snow_images) ? ' selected' : '' ?>@else value="snow10.png" style="background:url('image/snow10.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow11.png" style="background:url('image/snow11.png') no-repeat;" <?php echo in_array("snow11.png" , $snow_images) ? ' selected' : '' ?>@else value="snow11.png" style="background:url('image/snow11.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow12.png" style="background:url('image/snow12.png') no-repeat;" <?php echo in_array("snow12.png" , $snow_images) ? ' selected' : '' ?>@else value="snow12.png" style="background:url('image/snow12.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow13.png" style="background:url('image/snow13.png') no-repeat;" <?php echo in_array("snow13.png" , $snow_images) ? ' selected' : '' ?>@else value="snow13.png" style="background:url('image/snow13.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow14.png" style="background:url('image/snow14.png') no-repeat;" <?php echo in_array("snow14.png" , $snow_images) ? ' selected' : '' ?>@else value="snow14.png" style="background:url('image/snow14.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow15.png" style="background:url('image/snow15.png') no-repeat;" <?php echo in_array("snow15.png" , $snow_images) ? ' selected' : '' ?>@else value="snow15.png" style="background:url('image/snow15.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow16.png" style="background:url('image/snow16.png') no-repeat;" <?php echo in_array("snow16.png" , $snow_images) ? ' selected' : '' ?>@else value="snow16.png" style="background:url('image/snow16.png') no-repeat;" @endif></option>
                            <option class="zt-snow-image" @if(count($store_record)> 0) value="snow17.png" style="background:url('image/snow17.png') no-repeat;" <?php echo in_array("snow17.png" , $snow_images) ? ' selected' : '' ?>@else value="snow17.png" style="background:url('image/snow17.png') no-repeat;" @endif></option>
                        </select>                            
                    </div>
                </div>
            </div>
            
            <div class="left christmas-snowflake">
                <h2 class="sub-heading">Santa Settings</h2>
                    <div class="col-md-12 form-group section-group">                         
                        <div class="col-md-6 form-group">
                            <label for="show_santa">Show Santa?</label>
                            <select class="form-control" id="show_santa" name="show_santa">
                                <option @if(count($store_record)> 0) value="1" 
                                    <?php echo ($store_record->show_santa == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                                <option @if(count($store_record)> 0) value="0" 
                                    <?php echo ($store_record->show_santa == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                            </select>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <label for="santa_direction">Santa Direction</label>
                            <select class="form-control" id="santa_direction" name="santa_direction">
                                <option @if(count($store_record)> 0) value="bottom_left" 
                                    <?php echo ($store_record->santa_direction == 'bottom_left' ? ' selected' : '') ?>@else value="bottom_left" @endif>Bottom Left</option>
                                <option @if(count($store_record)> 0) value="bottom_right" 
                                    <?php echo ($store_record->santa_direction == 'bottom_right' ? ' selected' : '') ?>@else value="bottom_right" @endif>Bottom Right</option>
                                <option @if(count($store_record)> 0) value="top_left" 
                                    <?php echo ($store_record->santa_direction == 'top_left' ? ' selected' : '') ?>@else value="top_left" @endif>Top Left</option>
                                <option @if(count($store_record)> 0) value="top_right" 
                                    <?php echo ($store_record->santa_direction == 'top_right' ? ' selected' : '') ?>@else value="top_right" @endif>Top Right</option>
                            </select>
                        </div>
                        
                        <div class="col-md-12 form-group">
                            <label for="santa_image" class="zt-christmas">Select Santa Image</label>
                            <ul class="zt-ul-li">
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa1_image" @if(count($store_record)> 0) value="santa1.png" <?php echo ($store_record->santa_image == 'santa1.png' ? ' checked' : '') ?>@else value="santa1.png" @endif/>
                                        <label for="santa1_image"><img src="{!! asset('image/santa1.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa2_image" @if(count($store_record)> 0) value="santa2.png" <?php echo ($store_record->santa_image == 'santa2.png' ? ' checked' : '') ?>@else value="santa2.png" @endif/>
                                        <label for="santa2_image"><img src="{!! asset('image/santa2.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                                
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa3_image" @if(count($store_record)> 0) value="santa3.png" <?php echo ($store_record->santa_image == 'santa3.png' ? ' checked' : '') ?>@else value="santa3.png" @endif/>
                                        <label for="santa3_image"><img src="{!! asset('image/santa3.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa4_image" @if(count($store_record)> 0) value="santa4.png" <?php echo ($store_record->santa_image == 'santa4.png' ? ' checked' : '') ?>@else value="santa4.png" @endif/>
                                        <label for="santa4_image"><img src="{!! asset('image/santa4.png') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa5_image" @if(count($store_record)> 0) value="santa5.gif" <?php echo ($store_record->santa_image == 'santa5.gif' ? ' checked' : '') ?>@else value="santa5.gif" @endif/>
                                        <label for="santa5_image"><img src="{!! asset('image/santa5.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa6_image" @if(count($store_record)> 0) value="santa6.gif" <?php echo ($store_record->santa_image == 'santa6.gif' ? ' checked' : '') ?>@else value="santa6.gif" @endif/>
                                        <label for="santa6_image"><img src="{!! asset('image/santa6.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa7_image" @if(count($store_record)> 0) value="santa7.gif" <?php echo ($store_record->santa_image == 'santa7.gif' ? ' checked' : '') ?>@else value="santa7.gif" checked @endif/>
                                        <label for="santa7_image"><img src="{!! asset('image/santa7.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                                
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="santa_image" id="santa8_image" @if(count($store_record)> 0) value="santa8.gif" <?php echo ($store_record->santa_image == 'santa8.gif' ? ' checked' : '') ?>@else value="santa8.gif" @endif/>
                                        <label for="santa8_image"><img src="{!! asset('image/santa8.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
       
        <div class="col-md-6 left-christmas-snowflake">
            <div class="right christmas-snowflake">
                <h2 class="sub-heading">Shortcode</h2>
                <div class="success-copied"></div>
                <div class="view-shortcode">
                    <textarea id="ticker-shortcode" rows="2" class="form-control short-code"  readonly="">{% include 'christmas' %}</textarea>
                    <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe" style="display: block;">                        <i class="fa fa-check"></i>Copy to clipboard</button>
                </div>
                    
                <h5 class="sub-heading follow">Follow the Steps:</h5>
                <h6><b>Where to paste shortcode?</b></h6>
                    <ul class="limit" style="list-style: disc;">						
                        <li>After saving the details, you can copy the <b>Shortcode</b> from the options.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/dashboad-shortcode-shopify.png') }}"><b> See Example</b></a></li>
                        <li>After copy the short-code just open the <a href="https://<?php echo $store_name;?>/admin/themes/current/?key=layout/theme.liquid" target="_blank"><b>theme.liquid</b></a> and paste that code in anywhere into the page. <a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode.png') }}"><b>See Example</b></a></li>
                    </ul>
            </div>
                
                <div class="right christmas-snowflake">
                    <h2 class="sub-heading">Header Settings</h2>                
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-12 form-group">
                        <label for="show_header_garland">Show Header Garland?</label>
                        <select class="form-control zt-block-size" id="show_header_garland" name="show_header_garland">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_header_garland == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_header_garland == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                        </select>
                    </div>
                     
                    <div class="col-md-12 form-group">
                        <label for="header_garland_image" class="zt-christmas">Select Header Image</label>
                        <ul class="zt-ul-li">
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header1_garland_image" @if(count($store_record)> 0) value="header1.png" <?php echo ($store_record->header_garland_image == 'header1.png' ? ' checked' : '') ?>@else value="header1.png" checked @endif/>
                                    <label for="header1_garland_image"><img src="{!! asset('image/header1.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header2_garland_image" @if(count($store_record)> 0) value="header2.png" <?php echo ($store_record->header_garland_image == 'header2.png' ? ' checked' : '') ?>@else value="header2.png" @endif/>
                                    <label for="header2_garland_image"><img src="{!! asset('image/header2.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header3_garland_image" @if(count($store_record)> 0) value="header3.png" <?php echo ($store_record->header_garland_image == 'header3.png' ? ' checked' : '') ?>@else value="header3.png" @endif/>
                                    <label for="header3_garland_image"><img src="{!! asset('image/header3.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header4_garland_image" @if(count($store_record)> 0) value="header4.png" <?php echo ($store_record->header_garland_image == 'header4.png' ? ' checked' : '') ?>@else value="header4.png" @endif/>
                                    <label for="header4_garland_image"><img src="{!! asset('image/header4.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="header_garland_image" id="header5_garland_image" @if(count($store_record)> 0) value="header5.png" <?php echo ($store_record->header_garland_image == 'header5.png' ? ' checked' : '') ?>@else value="header5.png" @endif/>
                                    <label for="header5_garland_image"><img src="{!! asset('image/header5.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                    </ul>
                    </div>
                </div>
            </div>
            
            <div class="right christmas-snowflake">
                 <h2 class="sub-heading">Footer Settings</h2>                
                <div class="col-md-12 form-group section-group">
                    <div class="col-md-12 form-group">
                        <label for="show_footer_garland">Show Footer Garland</label>
                        <select class="form-control zt-block-size" id="show_footer_garland" name="show_footer_garland">
                            <option @if(count($store_record)> 0) value="1" 
                                <?php echo ($store_record->show_footer_garland == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                            <option @if(count($store_record)> 0) value="0" 
                                <?php echo ($store_record->show_footer_garland == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>                                      </select>
                    </div>                 
                    
                    <div class="col-md-12 form-group">
                        <label for="footer_garland_image" class="zt-christmas">Select Footer Image</label>
                        <ul class="zt-ul-li">
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="footer_garland_image" id="footer1_garland_image" @if(count($store_record)> 0) value="footer1.png" <?php echo ($store_record->footer_garland_image == 'footer1.png' ? ' checked' : '') ?>@else value="footer1.png" checked @endif/>
                                    <label for="footer1_garland_image"><img src="{!! asset('image/footer1.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="footer_garland_image" id="footer2_garland_image" @if(count($store_record)> 0) value="footer2.png" <?php echo ($store_record->footer_garland_image == 'footer2.png' ? ' checked' : '') ?>@else value="footer2.png" @endif/>
                                    <label for="footer2_garland_image"><img src="{!! asset('image/footer2.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                            
                            <li>
                                <div class="form-group">
                                    <input type="radio" name="footer_garland_image" id="footer3_garland_image" @if(count($store_record)> 0) value="footer3.png" <?php echo ($store_record->footer_garland_image == 'footer3.png' ? ' checked' : '') ?>@else value="footer3.png" @endif/>
                                    <label for="footer3_garland_image"><img src="{!! asset('image/footer3.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>

                            <li>
                                <div class="form-group">
                                    <input type="radio" name="footer_garland_image" id="footer4_garland_image" @if(count($store_record)> 0) value="footer4.png" <?php echo ($store_record->footer_garland_image == 'footer4.png' ? ' checked' : '') ?>@else value="footer4.png" @endif/>
                                    <label for="footer4_garland_image"><img src="{!! asset('image/footer4.png') !!}" class="zt-christmas-image"/></label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="right christmas-snowflake">                
                <h2 class="sub-heading">Flying Santa(Sleigh) Settings</h2>
                    <div class="col-md-12 form-group section-group">                         
                        <div class="col-md-6 form-group">
                            <label for="show_flying_santa">Show Flying Santa?</label>
                            <select class="form-control" id="show_flying_santa" name="show_flying_santa">
                                <option @if(count($store_record)> 0) value="1" 
                                    <?php echo ($store_record->show_flying_santa == '1' ? ' selected' : '') ?>@else value="1" @endif>Yes</option>
                                <option @if(count($store_record)> 0) value="0" 
                                    <?php echo ($store_record->show_flying_santa == '0' ? ' selected' : '') ?>@else value="0" @endif>No</option>
                            </select>
                        </div>
                        
                        <div class="col-md-6 form-group">
                            <label for="flying_santa_direction">Flying Santa Direction</label>
                            <select class="form-control" id="flying_santa_direction" name="flying_santa_direction">
                                <option @if(count($store_record)> 0) value="right_left" 
                                    <?php echo ($store_record->flying_santa_direction == 'right_left' ? ' selected' : '') ?>@else value="right_left" @endif>Right To Left</option>
                                <option @if(count($store_record)> 0) value="left_right" 
                                    <?php echo ($store_record->flying_santa_direction == 'left_right' ? ' selected' : '') ?>@else value="left_right" @endif>Left To Right</option>
                            </select>
                        </div>
                        
                        <div class="col-md-12 form-group">
                            <label for="flying_santa_image" class="zt-christmas">Select Flying Santa Image</label>
                            <ul class="zt-ul-li">
                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying1_santa_image" @if(count($store_record)> 0) value="flysanta1.gif" <?php echo ($store_record->flying_santa_image == 'flysanta1.gif' ? ' checked' : '') ?>@else value="flysanta1.gif" checked @endif/>
                                        <label for="flying1_santa_image"><img src="{!! asset('image/flysanta1.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying2_santa_image" @if(count($store_record)> 0) value="flysanta2.gif" <?php echo ($store_record->flying_santa_image == 'flysanta2.gif' ? ' checked' : '') ?>@else value="flysanta2.gif" @endif/>
                                        <label for="flying2_santa_image"><img src="{!! asset('image/flysanta2.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">                                
                                        <input type="radio" name="flying_santa_image" id="flying3_santa_image" @if(count($store_record)> 0) value="flysanta3.gif" <?php echo ($store_record->flying_santa_image == 'flysanta3.gif' ? ' checked' : '') ?>@else value="flysanta3.gif" @endif/>
                                        <label for="flying3_santa_image"><img src="{!! asset('image/flysanta3.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying4_santa_image" @if(count($store_record)> 0) value="flysanta4.gif" <?php echo ($store_record->flying_santa_image == 'flysanta4.gif' ? ' checked' : '') ?>@else value="flysanta4.gif" @endif/>
                                        <label for="flying4_santa_image"><img src="{!! asset('image/flysanta4.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>

                                <li>
                                    <div class="form-group">
                                        <input type="radio" name="flying_santa_image" id="flying5_santa_image" @if(count($store_record)> 0) value="flysanta5.gif" <?php echo ($store_record->flying_santa_image == 'flysanta5.gif' ? ' checked' : '') ?>@else value="flysanta5.gif" @endif/>
                                        <label for="flying5_santa_image"><img src="{!! asset('image/flysanta5.gif') !!}" class="zt-christmas-image"/></label>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>                
            </div>
        </div>       
    </form>
    </div>  
</div>

<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
      <div class="modal-dialog" style="width: 80%;">
       <div class="modal-content">              
        <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
         </button>
         <img src="" class="imagepreview" style="width: 100%;">
        </div>
       </div>
      </div>
     </div>
@endsection
